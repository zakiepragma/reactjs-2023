function App() {
  return (
    <div className={'bg-yellow-200 grid place-content-center min-h-screen'}>
      <a
        href='#'
        class='group block max-w-xl mx-auto rounded-lg p-6 bg-white ring-green-300 ring-1 ring-slate-900/5 shadow-lg space-y-3 hover:bg-yellow-300 hover:ring-sky-500'
      >
        <blockquote class='text-2xl font-semibold italic text-center text-slate-900'>
          Tauhid
          <span class='before:block before:absolute before:-inset-0 before:-skew-y-3 before:bg-blue-500 relative inline-block'>
            <span class='relative text-white'>pertama</span>
          </span>
          dan 
          <span class='before:block before:absolute before:-inset-0 before:-skew-y-3 before:bg-pink-500 relative inline-block'>
            <span class='relative text-white'>utama</span>
          </span>
        </blockquote>
      </a>
    </div>
  )
}

export default App
